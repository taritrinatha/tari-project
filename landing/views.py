from django.shortcuts import render
from django.shortcuts import redirect
from .forms import StatusForm
from .models import Status

def home(request):
	data = Status.objects.all().order_by('-dates')
	form = StatusForm()

	content = {'title' : 'My Status',
		 'form' : form,
		 'text' : 'Halo, apa kabar?',
		 'data' : data}
	return render(request, 'index.html', content)

def add_status(request):
	form = StatusForm(request.POST)
	if form.is_valid():
		form.save()
	return redirect('home')

def profile(request):
	return render(request, 'profile.html')
