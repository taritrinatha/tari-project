from django.db import models
from datetime import datetime

class Status(models.Model):
    dates = models.DateTimeField(default= datetime.now, blank=True)
    status = models.TextField(max_length=300)
