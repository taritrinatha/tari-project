from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.utils import timezone
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import *
from .models import Status
from .forms import StatusForm
from datetime import date
import time

class Story6_Web_Test(LiveServerTestCase):
    def test_url_is_exist(self):
        response = Client().get('/landing/')
        self.assertEqual(response.status_code, 200)

    def test_using_landing_template(self):
        response = Client().get('/landing/')
        self.assertTemplateUsed(response, 'index.html')

    def test_using_home_func(self):
        found = resolve('/landing/')
        self.assertEqual(found.func, home)

    def test_helo(self):
        request = HttpRequest()
        response = home(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Halo, apa kabar?', html_response)

class Story6_Model_Test(LiveServerTestCase):
    def test_model_create_new_status(self):
        new_status = Status.objects.create(dates=timezone.now(), status='Coba Coba')

        counting_status_object = Status.objects.all().count()
        self.assertEqual(counting_status_object, 1)

    def test_save_POST_request(self):
        response = self.client.post('/landing/add_status/', data={'dates' : '2018-10-10T14:46', 'status' : 'Coba Coba'})
        counting_status_object = Status.objects.all().count()
        self.assertEqual(counting_status_object, 1)

        self.assertEqual(response.status_code, 302)

        new_response = self.client.get('/landing/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Coba Coba', html_response)

class Story6_Form_Test(LiveServerTestCase):
    def test_forms_valid(self):
        form_data = {'status': 'Coba Coba'}
        form = StatusForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_forms_not_valid(self):
        form_data = {'status': 'One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly. Meh'}
        form = StatusForm(data=form_data)
        self.assertFalse(form.is_valid())

class Story6_Functional_Test(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium  = webdriver.Chrome(chrome_options=chrome_options)
        super(Story6_Functional_Test,self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story6_Functional_Test, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/landing/')
        time.sleep(5);
        status = selenium.find_element_by_name('status')
        submit = selenium.find_element_by_id('submit')

        status_message = 'Coba Coba'
        status.send_keys(status_message)
        submit.click()
        time.sleep(10)

    def test_element_html_1(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/landing/')
        time.sleep(5);
        title = selenium.find_element_by_tag_name('h1')
        self.assertEqual(title.text, 'My Status')

    def test_css_1(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/landing/')
        time.sleep(5);
        css = selenium.find_element_by_class_name('left')
        self.assertEqual(selenium.find_element_by_id('form'), css)

    def test_change_theme_background(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/landing/')
        time.sleep(10);
        css = selenium.find_element_by_tag_name('body')
        background1 = css.value_of_css_property('background')

        button = selenium.find_element_by_id('theme')
        button.click()

        background2 = css.value_of_css_property('background')
        self.assertNotEqual(background1, background2)


    def test_accordion(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/landing/profile/')
        time.sleep(10);
        accord = selenium.find_element_by_class_name('accordion')
        panel = selenium.find_element_by_class_name('panel')

        thedisplay = panel.value_of_css_property('display')
        self.assertEqual(thedisplay, 'none')

        accord.click()

        thedisplay = panel.value_of_css_property('display')
        self.assertEqual(thedisplay, 'block')
