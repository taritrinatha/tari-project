$(document).ready(function() {
  var allPanels = $('.panel').hide();
  var active;
  var clicked = false;

  $(document).on('click', '.accordion', function() {
    allPanels.slideUp();
    if(this === active){
      $(this).next().slideUp();
      $(this).removeClass('active');
      active = null;
      return false;
    }
    $(active).removeClass('active');
    $(this).addClass('active');
    $(this).next().slideDown();
    active = this;
    return false;
  });

  $('#theme').on('click', function() {
      if(clicked){
        $('.header').css('background-color', '#171B1A');
        $('body').css('background-color', '#f2f6e9');
        $('.panel').css('background-color', '#f2f6e9');
	$('.accordion').css('background', '#5E8996');
	$('.accordion').css('color', '#ffffff');
        $('body').css('color', 'black');
        $('button').css('color', 'white');
        $('#theme > h6').text('Dark mode');
        clicked = false;
      }else{
        $('.header').css('background-color', '#000000');
        $('body').css('background-color', '#142634');
        $('.panel').css('background-color', '#142634');
        $('.accordion').css('background', '#ffffff');
	$('.accordion').css('color', '#171B1A');
        $('body').css('color', 'white');
        $('#theme > h6').text('Bright mode');
        clicked = true;
      };
      return false;
  });
});
