from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class Story8_Unit_Test(LiveServerTestCase):
     def test_url_is_exist(self):
         response = Client().get('/story8/')
         self.assertEqual(response.status_code, 200)

     def test_using_landing_template(self):
         response = Client().get('/story8/')
         self.assertTemplateUsed(response, 'home.html')

class Story8_Json_Test(LiveServerTestCase):
     def test_url_json_exist(self):
         response = Client().get('/story8/books/getbook/science')
         self.assertEqual(response.status_code, 200)

     def test_using_json_func(self):
         found = resolve('/story8/books/getbook/science')
         self.assertEqual(found.func, readJSON)

class Story8_Functional_Test(LiveServerTestCase):
     def setUp(self):
         chrome_options = Options()
         chrome_options.add_argument('--dns-prefetch-disable')
         chrome_options.add_argument('--no-sandbox')
         chrome_options.add_argument('--headless')
         chrome_options.add_argument('--disable-gpu')
         self.selenium  = webdriver.Chrome(chrome_options=chrome_options)
         super(Story8_Functional_Test,self).setUp()

     def tearDown(self):
         self.selenium.quit()
         super(Story8_Functional_Test, self).tearDown()

     def test_change_theme_background(self):
         selenium = self.selenium
         selenium.get(self.live_server_url + '/story8/')
         time.sleep(5);
         css = selenium.find_element_by_tag_name('body')
         background1 = css.value_of_css_property('background')
         button = selenium.find_element_by_id('theme')
         button.click()

         background2 = css.value_of_css_property('background')
         self.assertNotEqual(background1, background2)
