from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from django.http import HttpResponseRedirect
import json
import requests
from django.forms.models import model_to_dict
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

url = 'https://www.googleapis.com/books/v1/volumes?q='

def home(request):
	return render(request, 'home.html')

@csrf_exempt
def books(request):
	content = {'title' : 'Recommendations'}
	return render(request, 'books.html', content)

def readJSON(request, book_name):
	response = requests.get(url + book_name)
	data = response.json()
	books = {
		'book' : []
	}

	for item in data['items']:
		temp = {}
		temp['title'] = item['volumeInfo']['title']
		temp['authors'] = item['volumeInfo'].get('authors', ["Anonim"])[0]
		temp['images'] = item['volumeInfo']['imageLinks']['thumbnail']
		books['book'].append(temp)

	json_data = json.dumps(books)

	return HttpResponse(json_data, content_type="application/json")
