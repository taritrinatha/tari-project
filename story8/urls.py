from django.urls import path, include
from .views import *

urlpatterns = [
    path('', home, name='home'),
    path('books/', books, name='books'),
    path('books/getbook/<book_name>', readJSON, name='json'),
]
