$(document).ready(function(){
    var book_name = $('#search_input').val();
    var url = "getbook/";

    load = function(book_name){
      $.ajax({
      type: "GET",
      url: url + book_name,
      dataType: 'json',
      success: function(response){
        $('#table_book > tbody').empty();
        data = response;
        book = data.book;

        for(i = 0; i < book.length; i++){
            var info = book[i]
            var tmp = "<tr><td id='title'>" + info.title + "</td><td>" + info.authors + "</td>" +
            "<td><img src="+info.images+"></td></tr>";
            $('#table_book > tbody').append(tmp);
          }
        }
      });
    }

    load('science');

    $(document).on('click', '#search', function(){
      var book_name_local = $('#search_input').val();
      load(book_name_local);
    });

    $(document).keypress(function(e){
      if(e.which === 13){
        $('#search').click();
      }
    });

        return false;

})
